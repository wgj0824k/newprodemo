package com.wgj.newprodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewprodemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewprodemoApplication.class, args);
	}

}
